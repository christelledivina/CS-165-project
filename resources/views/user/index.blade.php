@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">User Profile</div>

                <div class="panel-body">
                    <ul>
                        <li>Name: {{Auth::user()->name}}</li>
                        <li>Username: {{Auth::user()->username}}</li>
                    </ul>
                </div>
                <table class="table">
                    <thead></thead>
                    <tbody>
                    <tr>
                        <td colspan="4" align="center"><a href="/user/update" class="btn btn-raised btn-primary">Update profile</a></td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection
