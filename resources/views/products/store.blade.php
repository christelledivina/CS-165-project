@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Products</div>

                <div class="panel-body">
                    Your product has been successfully added!
                </div>
                <table class="table">
                    <thead></thead>
                    <tbody>
                    <tr>
                        <td colspan="4" align="center"><a href="/products" class="btn btn-raised btn-primary">Back to products</a></td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection
