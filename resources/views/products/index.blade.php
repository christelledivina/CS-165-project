@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Products</div>
                
                @if($products->count())
                <table class="table">
                    <thead>
                        <tr>
                            <th align="center">Wand Wood</th>
                            <th align="center">Garrick Ollivander's notes</th>
                            <th align="center">Status</th>
                            <th align="center">Available Items</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($products as $product)
                        <tr>
                            <td>{{$product->item}}</td>
                            <td>{{$product->description}}</td>
                            <td>{{$product->status}}</td>
                            <td>{{$product->items_available}}</td>
                            <td><a href="/cart/additem/{{$product->id}}" class="btn btn-raised btn-primary">Add to cart</a></td>
                        </tr>
                        @endforeach
                    </tbody> 
                </table>
                @else
                    There are no products.
                @endif
                @if(Auth::user()->is_admin)
                <table class="table">
                    <thead></thead>
                    <tbody>
                    <tr>
                        <td colspan="4" align="center"><a href="/products/create" class="btn btn-raised btn-primary">Add a product</a></td>
                    </tr>
                    </tbody>
                </table>
                @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
