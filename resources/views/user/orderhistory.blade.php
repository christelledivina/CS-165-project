@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Order History</div>
                
                @if($cart->count())
                <table class="table">
                    <thead>
                        <tr>
                            <th align="center">Time Ordered</th>
                            <th align="center">Quantity</th>
                            <th align="center">Wand</th>
                            <th align="center">Garrick Ollivander's notes</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($cart as $cart_item)
                            @if($cart_item->submitted)
                            <tr>
                                <td>{{$cart_item->created_at}}</td>
                                <td>{{$cart_item->quantity}}</td>
                                <td>{{$cart_item->product->item}}</td>
                                <td>{{$cart_item->product->description}}</td>
                            </tr>
                            @endif
                        @endforeach
                    </tbody> 
                </table>
                @else
                    There are no orders.
                @endif
                <table class="table">
                    <thead></thead>
                    <tbody>
                    <tr>
                        <td colspan="4" align="center"><a href="/" class="btn btn-raised btn-primary">Back to Home</a></td>
                    </tr>
                    </tbody>
                </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
