<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Product;
use App\Cart;
use Carbon;
use View;
use DB;
use Form;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    public function index(){
    	$users = DB::table('users')->get();
    	$users->toarray();

    	return View::make('user.index')->with('users', $users);
    }

    public function update(){
    	return view('user.update');
    }

    public function store(Request $request){
    	$user = User::find(Auth::id());
    	$username = $request->input('username');
    	$name = $request->input('name');
    	$user->username = $username;
    	$user->name = $name;
    	$user->save();

    	return View::make('user.store');
    }

    public function orderHistory(){
        $cart = Cart::where('user_id',Auth::user()->id)->first();
 
        if(!$cart){
            $cart = new Cart();
            $cart->user_id = Auth::user()->id;
            $cart->save();
        }
 
        $items = $cart->cartItems;

        return View::make('user.orderhistory')->with('cart', $items);
    }
}
