<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index');

Auth::routes();

Route::get('/view-cart', function () {
	return view('view-cart');
});

Auth::routes();

Route::get('/products', 'ProductsController@index');

Auth::routes();

Route::group(['middleware' => ['auth']], function(){
	Route::resource('products', 'ProductsController');
	Route::get('/cart/additem/{productId}', 'CartController@addItem');
	Route::get('/cart/removeitem/{productId}', 'CartController@removeItem');
	Route::get('/cart', 'CartController@showCart');
	Route::get('/cart/submit', 'CartController@submit');
	Route::get('/user', 'UserController@index');
	Route::get('/user/update', 'UserController@update');
	Route::post('/user/store', 'UserController@store');
	Route::get('/updateitem/{productId}', 'CartController@updateItem');
	Route::post('/updatestore/{productId}', 'CartController@updateStore');
	Route::get('/user/orderhistory', 'UserController@orderHistory');
});