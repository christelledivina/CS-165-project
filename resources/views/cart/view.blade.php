@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">View Cart</div>
                
                @if($items->count())
                <table class="table">
                    <thead>
                        <tr>
                            <th> </th>
                            <th align="center">Wand</th>
                            <th align="center">Quantity</th>
                            <th> </th>
                            <th> </th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($items as $item)
                            @if(!($item->submitted))
                            <tr>
                                <td> </td>
                                <td>{{$item->product->item}}</td>
                                <td>{{$item->quantity}}</td>
                                <td> </td>
                                <td align="right"><a href="/updateitem/{{$item->product_id}}" class="btn btn-raised btn-primary">Update item</a></td>
                                <td align="center"><a href="/cart/removeitem/{{$item->product_id}}"> <button type="button" class="btn btn-danger">
                                        <span class="fa fa-remove"></span> Remove
                                    </button>
                                </a></td>
                            </tr>
                            @endif
                        @endforeach
                    </tbody> 
                </table>
                @else
                    Your cart is empty.
                @endif
                <table class="table">
                    <thead></thead>
                    <tbody>
                    <tr>
                        <td colspan="4" align="center"><a href="/cart/submit" class="btn btn-raised btn-primary">Submit Cart</a></td>
                    </tr>
                    </tbody>
                </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
