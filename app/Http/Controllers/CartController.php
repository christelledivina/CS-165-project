<?php

namespace App\Http\Controllers;

use App\Cart;
use App\CartItem;
use App\Product;
use DB;
use View;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use \Illuminate\Http\Response;
use App\Http\Controllers\Controller;

class CartController extends Controller
{
    public function addItem($productId){
 
        $cart = Cart::where('user_id', Auth::user()->id)->first();
 
        if(!$cart){
            $cart =  new Cart();
            $cart->user_id=Auth::user()->id;
            $cart->save();
        }

 		$i = CartItem::where('product_id', $productId)->first();
 		if($i and $i->submitted == False){
 			DB::table('cart_items')->where('product_id', $productId)->increment('quantity');
 		}
 		else{
	        $cartItem  = new Cartitem();
	        $cartItem->product_id = $productId;
	        $cartItem->cart_id = $cart->id;
	        $cartItem->quantity = 1;
	        $cartItem->submitted = False;
	        $cartItem->save();
		}	        

        DB::table('products')->where('id', '=', $productId)->decrement('items_available');

        $check = DB::table('products')->where('id', '=', $productId)->get();
        if($check->items_available = 0){
        	DB::table('products')->where('id', '=', $productId)->update(['status' => 'out of stock']);
        }
 
        return redirect('/cart');


 
    }

    public function submit(){
    	$cart = Cart::where('user_id', Auth::user()->id)->first();
    	DB::table('cart_items')->where([['cart_id', $cart->id], ['submitted', False]])->update(['submitted' => True]);

    	return View::make('cart.submit');

    }

    public function updateItem($productId){
        return View::make('cart.update')->with('product_id', $productId);
    }

    public function updateStore(Request $request, $productId){
    	$olditem = DB::table('cart_items')->where([['product_id', $productId], ['submitted', False]])->value('quantity');
    	$check = DB::table('products')->where('id', $productId)->value('items_available');

    	if($olditem < $request->input('quantity')){
    		$i = $request->input('quantity') - $olditem;
    		if($i > $check){
    			return redirect('/cart');
    		}
    		DB::table('cart_items')->where('product_id', $productId)->increment('quantity', $i);
    		DB::table('products')->where('id', $productId)->decrement('items_available', $i);
    	}
    	else{
    		DB::table('cart_items')->where('product_id', $productId)->decrement('quantity', $i);
    		DB::table('products')->where('id', $productId)->increment('items_available', $i);
    	}

    	return view('cart.store');
    }
 
    public function showCart(){
        $cart = Cart::where('user_id',Auth::user()->id)->first();
 
        if(!$cart){
            $cart = new Cart();
            $cart->user_id = Auth::user()->id;
            $cart->save();
        }
 
        $items = $cart->cartItems;
 
        return view('cart.view',['items' => $items]);
    }
 
    public function removeItem($productId){
        $i = CartItem::where('product_id', $productId)->first();

        DB::table('products')->where('id', $productId)->increment('items_available', $i->quantity);

        CartItem::destroy($i->id);

        return redirect('/cart');
    }
}
